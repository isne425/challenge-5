#include<iostream>

using namespace std;

void main()
{
	char s1[50]; // store input sentence 
	cout << "Enter a sentence : ";
	cin.getline(s1,50);

	int size = strlen(s1); // numbers of alphabets

	char s2[50]; // store inverted input sentence
	for (int i = 0; i < size; i++)
	{
		s2[i] = s1[size - i - 1]; // invertion 
	}
	
	int count=0; // store numbers of the same character in each elements 
	for (int i = 0; i < size; i++)
	{
		if (s1[i] == s2[i])
		{
			count++;
		}
		else
		{
			count = 0;
		}
	}

	if (count == size) // inverted input == original input
	{
		cout << "It is palindrome." << endl;
	}
	else
	{
		cout << "It is not palindrome." << endl;
	}

	system("pause");
}